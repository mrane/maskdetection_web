// import 'dart:async';
// import 'dart:convert';
// import 'dart:html';
// // ignore: avoid_web_libraries_in_flutter
// import 'dart:html' as html;
// import 'dart:js' as js;
//
// import 'package:flutter/material.dart';
// import 'package:fluttertoast/fluttertoast.dart';
// import 'package:get/get.dart';
// import 'package:intl/intl.dart';
// import 'package:jiovms_kiosk_web/app/bloc/save_details_bloc.dart';
// import 'package:jiovms_kiosk_web/app/bloc/visitor_details_bloc.dart';
// import 'package:jiovms_kiosk_web/app/data/models/qr_scan_model.dart';
// import 'package:jiovms_kiosk_web/app/data/models/save_details_model.dart';
// import 'package:jiovms_kiosk_web/app/data/models/visitor_details_model.dart';
// import 'package:jiovms_kiosk_web/app/data/provider/custom_exception.dart';
// import 'package:jiovms_kiosk_web/app/features/helper/analytics_helper/analytics_event.dart';
// import 'package:jiovms_kiosk_web/app/features/helper/qr_helper/qr_helper.dart';
// import 'package:jiovms_kiosk_web/app/router/app_pages.dart';
// import 'package:jiovms_kiosk_web/app/util/utils.dart';
// import 'package:shared_preferences/shared_preferences.dart';
//
// class QrController extends GetxController {
//   bool isMobile;
//   final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
//   var qrText = "";
//   var qrString;
//   VisitorDetailsBloc _visitorDetailsBloc;
//   bool isBarcodeRecognized = false;
//   SaveDetailsBloc _saveDetailsBloc;
//   BuildContext context;
//   SharedPreferences prefs;
//   RxString companyLogo = "".obs;
//   QrScanModel qrScanModel;
//   var acsColor = "".obs;
//   DateTime qrScanStartTime;
//
//   QrController(this.context);
//
//   @override
//   void onInit() {
//     super.onInit();
//     QRHelper.init();
//     QRHelper.qrVideoElement.autoplay = true;
//     initializeData();
//     html.window.addEventListener("qrStringFound", (event) {
//       CustomEvent qrEvent = event;
//       qrString = qrEvent.detail;
//       callQRFound(qrString);
//     });
//     _visitorDetailsBloc = VisitorDetailsBloc();
//     _saveDetailsBloc = SaveDetailsBloc();
//   }
//
//   @override
//   void dispose() {
//     super.dispose();
//     html.window.removeEventListener('qrStringFound', (event) => {});
//   }
//
//   Future<void> initializeData() async {
//     prefs = await SharedPreferences.getInstance();
//     acsColor.value = prefs.getString('acscolor');
//     companyLogo.value = prefs.getString('companyLogo');
//     update();
//   }
//
//   Future<void> callVisitorDetailsApi() async {
//     prefs = await SharedPreferences.getInstance();
//     qrScanModel =
//         QrScanModel.fromJson(jsonDecode(prefs.getString('qrScanModel')));
//
//     var headers = {"idtoken": prefs.getString('idToken')};
//
//     var params = {
//       "visitorid": {
//         "iv": qrScanModel.visitorid.iv,
//         "content": qrScanModel.visitorid.content
//       },
//       "compid": {
//         "iv": qrScanModel.compid.iv,
//         "content": qrScanModel.compid.content
//       },
//       "siteid": {
//         "iv": qrScanModel.siteid.iv,
//         "content": qrScanModel.siteid.content
//       },
//       "emailid": {
//         "iv": qrScanModel.emailid.iv,
//         "content": qrScanModel.emailid.content
//       },
//       "mobno": {
//         "iv": qrScanModel.mobno.iv,
//         "content": qrScanModel.mobno.content
//       },
//       "deviceno": prefs.getString('deviceId')
//     };
//     if (await (Utils.checkInternetAndShowMsg()) == true) {
//       Utils.showLoadingDialog(context);
//       dynamic response =
//           await _visitorDetailsBloc.visitorDetails(headers, params);
//       Utils.dismissLoadingDialog(context);
//       if (response["status"] == 200) {
//         DateTime qrScanEndTime = DateTime.now();
//         AnalyticsEvent.fireAnalyticsEventQrcodeMatch(
//             true, qrScanEndTime.difference(qrScanStartTime).inSeconds, "");
//         prefs = await SharedPreferences.getInstance();
//         Fluttertoast.showToast(msg: "QR validated successfully");
//         prefs.setBool('isCheckedIn', true);
//         VisitorDetailsModel visitorDetailsModel =
//             VisitorDetailsModel.fromJson(response);
//         dynamic visitor = visitorDetailsModel.result[0][0];
//         // DataManager.get().visitorResultMap = visitor;
//         prefs.setString('visitorResultMap', jsonEncode(visitor));
//         VisitorResult visitorResult =
//             VisitorResult.fromJson(visitorDetailsModel.result[0][0]);
//         // DataManager.get().visitorResult = visitorResult;
//         prefs.setString('visitorResult', jsonEncode(visitorResult));
//
//         dynamic hostResult = visitorDetailsModel.result[1];
//         // DataManager.get().hostResult = hostResult;
//
//         prefs.setString('hostResult', jsonEncode(hostResult));
//
//         var formatted = DateFormat("d MMM y")
//             .format(DateTime.parse(hostResult[0]["date"]))
//             .toString();
//         Get.offNamed(Routes.visitorDetailsPage);
//       } else if (response["status"] == 409) {
//         DateTime qrScanEndTime = DateTime.now();
//         AnalyticsEvent.fireAnalyticsEventQrcodeMatch(
//             false,
//             qrScanEndTime.difference(qrScanStartTime).inSeconds,
//             response["message"] ?? CustomException.ERROR_CRASH_MSG);
//         Get.offAllNamed(Routes.loginPage);
//       } else {
//         DateTime qrScanEndTime = DateTime.now();
//         AnalyticsEvent.fireAnalyticsEventQrcodeMatch(
//             false,
//             qrScanEndTime.difference(qrScanStartTime).inSeconds,
//             response["message"] ?? CustomException.ERROR_CRASH_MSG);
//         isBarcodeRecognized = false;
//         update();
//         Fluttertoast.showToast(
//             msg: response["message"] ?? CustomException.ERROR_CRASH_MSG);
//         Get.offAllNamed(Routes.checkinPage);
//       }
//     }
//   }
//
//   Future<void> callSaveDetailsApi(bool isCheckIn) async {
//     prefs = await SharedPreferences.getInstance();
//     var headers = {"idtoken": prefs.getString('idToken')};
//     // var headers = DataManager.headers;
//     qrScanModel =
//         QrScanModel.fromJson(jsonDecode(prefs.getString('qrScanModel')));
//     var params = {
//       "visitorid": {
//         "iv": qrScanModel.visitorid.iv,
//         "content": qrScanModel.visitorid.content
//       },
//       "compid": {
//         "iv": qrScanModel.compid.iv,
//         "content": qrScanModel.compid.content
//       },
//       "siteid": {
//         "iv": qrScanModel.siteid.iv,
//         "content": qrScanModel.siteid.content
//       },
//       "emailid": {
//         "iv": qrScanModel.emailid.iv,
//         "content": qrScanModel.emailid.content
//       },
//       "mobno": {
//         "iv": qrScanModel.mobno.iv,
//         "content": qrScanModel.mobno.content
//       },
//       "flag": "OUT",
//       "deviceno": prefs.getString('deviceId')
//     };
//     if (await (Utils.checkInternetAndShowMsg()) == true) {
//       Utils.showLoadingDialog(context);
//       dynamic response = await _saveDetailsBloc.saveDetails(headers, params);
//       Utils.dismissLoadingDialog(context);
//       if (response["status"] == 200) {
//         DateTime qrScanEndTime = DateTime.now();
//         AnalyticsEvent.fireAnalyticsEventQrcodeMatch(
//             true, qrScanEndTime.difference(qrScanStartTime).inSeconds, "");
//         //prefs.setString("checkOutEnd", DateTime.now().toString());
//         DateTime checkOutEnd = DateTime.now();
//         DateTime checkOutStart =
//             DateTime.parse(prefs.getString("checkOutStart"));
//         int checkOutProcessTime =
//             checkOutEnd.difference(checkOutStart).inSeconds;
//         AnalyticsEvent.fireAnalyticsEventCheckOut(checkOutProcessTime);
//         SaveDetailsModel saveDetailsModel = SaveDetailsModel.fromJson(response);
//         Fluttertoast.showToast(msg: saveDetailsModel.result[0].msg);
//         prefs.setBool('isCheckedIn', false);
//         Get.offAllNamed(Routes.thankyoupage);
//       } else if (response["status"] == 409) {
//         DateTime qrScanEndTime = DateTime.now();
//         AnalyticsEvent.fireAnalyticsEventQrcodeMatch(
//             false,
//             qrScanEndTime.difference(qrScanStartTime).inSeconds,
//             response["message"] ?? CustomException.ERROR_CRASH_MSG);
//         Get.offAllNamed(Routes.loginPage);
//       } else {
//         DateTime qrScanEndTime = DateTime.now();
//         AnalyticsEvent.fireAnalyticsEventQrcodeMatch(
//             false,
//             qrScanEndTime.difference(qrScanStartTime).inSeconds,
//             response["message"] ?? CustomException.ERROR_CRASH_MSG);
//         (response["message"] != null)
//             ? Fluttertoast.showToast(msg: response["message"])
//             : Fluttertoast.showToast(msg: CustomException.ERROR_CRASH_MSG);
//         Get.offAllNamed(Routes.checkinPage);
//       }
//     }
//   }
//
//   void callQRFound(var qrString) {
//     if (validateQRCodeSring(qrString) && !isBarcodeRecognized) {
//       qrText = qrString;
//       isBarcodeRecognized = true;
//       qrScannedSuccessfully(qrString);
//     }
//   }
//
//   bool validateQRCodeSring(String qrString) {
//     if (qrString.contains("q1") &&
//         qrString.contains("q2") &&
//         qrString.contains("q3") &&
//         qrString.contains("q4") &&
//         qrString.contains("q5")) {
//       return true;
//     }
//     return false;
//   }
//
//   void routeAndStopCamera() {
//     js.context.callMethod('stopScanning');
//     stopStreamedVideo(QRHelper.qrVideoElement);
//     Get.offNamed(Routes.enterVisitorCodePage);
//   }
//
//   Future<void> qrScannedSuccessfully(String qrResult) async {
//     qrScanStartTime = DateTime.now();
//     try {
//       prefs = await SharedPreferences.getInstance();
//       stopStreamedVideo(QRHelper.qrVideoElement);
//       Map result = json.decode(qrResult);
//       QrScanModel qrScanModel = QrScanModel.fromJson(result);
//       // DataManager.get().qrScanModel = qrScanModel;
//       prefs.setString('qrScanModel', jsonEncode(qrScanModel));
//       //      Fluttertoast.showToast(msg:"DataManager.get().isCheckedin: ${DataManager.get().isCheckedin}");
//       if (prefs.getBool('isCheckedIn')) {
//         if (await (Utils.checkInternetAndShowMsg()) == true) {
//           // commented so that qr doesn't expire remove route when api is called
//           callSaveDetailsApi(false);
//         }
//       } else {
//         if (await (Utils.checkInternetAndShowMsg()) == true) {
//           callVisitorDetailsApi();
//         }
//       }
//     } catch (e) {
//       print(e.toString());
//     }
//   }
//
//   stopStreamedVideo(VideoElement videoElem) {
//     if (videoElem.srcObject != null) {
//       List<MediaStreamTrack> tracks = videoElem.srcObject.getTracks();
//       for (MediaStreamTrack track in tracks) {
//         track.stop();
//       }
//     }
//     videoElem.srcObject = null;
//     // videoElem.removeEventListener('play', (event) => {});
//   }
// }
