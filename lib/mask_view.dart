import 'dart:html' as html;
import 'dart:html';

import 'package:facemaskdetection_web/page2.dart';
import 'package:facemaskdetection_web/video_helper.dart';
import 'package:flutter/material.dart';

class MaskScanWebPage extends StatefulWidget {
  @override
  _MaskScanWebPageState createState() => _MaskScanWebPageState();
}

class _MaskScanWebPageState extends State<MaskScanWebPage> {
  String maskDetectionMsg = "";
  bool success;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    QRHelper.init();
    QRHelper.qrVideoElement.autoplay = true;

    html.window.addEventListener("maskDetectionSuccess", (event) {
      maskDetectionMsg = "Mask Found";
      success = true;
      setState(() {});
    });

    html.window.addEventListener("maskDetectionFail", (event) {
      maskDetectionMsg = "Mask Not Found";
      success = false;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.lightBlue,
      body: SingleChildScrollView(
        child: Container(
          child: getUiForWeb(),
        ),
      ),
    );
  }

  stopStreamedVideo(VideoElement videoElem) {
    if (videoElem.srcObject != null) {
      List<MediaStreamTrack> tracks = videoElem.srcObject.getTracks();
      for (MediaStreamTrack track in tracks) {
        track.stop();
      }
    }
    videoElem.srcObject = null;
    //videoElem.removeEventListener('play', (event) => {});
  }

  Widget getUiForWeb() {
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: 100,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Scan QR Code"),
                ],
              ),
              SizedBox(
                width: 50,
              ),
              Container(
                width: 2,
                height: 350,
              ),
              SizedBox(
                width: 50,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Container(
                    padding: EdgeInsets.all(15.0),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(13)),
                    ),
                    child: Center(child: QRHelper.QRWidget
                        //container for qr cam
                        ),
                    height: 287,
                    width: 287,
                  ),
                  SizedBox(
                    height: 27,
                  ),
                  Text(
                    maskDetectionMsg,
                    style: TextStyle(color: Colors.black, fontSize: 30),
                  ),
                  SizedBox(
                    height: 50,
                  ),
                  RaisedButton(
                    child: Text('Next'),
                    onPressed: () {
                      //stopStreamedVideo(QRHelper.qrVideoElement);
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Page2()));
                    },
                  )
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
