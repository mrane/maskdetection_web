import 'dart:html' as html;

import 'package:facemaskdetection_web/page3.dart';
import 'package:facemaskdetection_web/video_helper.dart';
import 'package:flutter/material.dart';

class Page2 extends StatefulWidget {
  @override
  _Page2State createState() => _Page2State();
}

class _Page2State extends State<Page2> {
  String maskDetectionMsg = "";
  bool success = true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    QRHelper.init();
    QRHelper.qrVideoElement.autoplay = true;

    html.window.addEventListener("maskDetectionSuccess", (event) {
      maskDetectionMsg = "Mask Found";
      success = true;
      //QRHelper.qrVideoElement.autoplay = true;
      //print("Success = $success");
      setState(() {});
    });

    html.window.addEventListener("maskDetectionFail", (event) {
      maskDetectionMsg = "Mask Not Found";
      success = false;
      //print("Success = $success");
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.lightBlue,
        body: Stack(
          children: [
            Align(
              alignment: Alignment.bottomRight,
              child: Container(
                color: Colors.white,
                child: Center(
                    //
                    child: QRHelper.QRWidget),
                height: 100,
                width: 100,
              ),
            ),
            getUiForWeb(),
          ],
        ),
      ),
    );
  }

  Widget getUiForWeb() {
    return success
        ? Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: 100,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text("Page 2"),
                    ],
                  ),
                  SizedBox(
                    width: 50,
                  ),
                  Container(
                    width: 2,
                    height: 350,
                  ),
                  RaisedButton(
                    child: Text('Next'),
                    onPressed: () {
                      //stopStreamedVideo(QRHelper.qrVideoElement);
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => Page3()));
                    },
                  )
                ],
              ),
            ],
          )
        : Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: 100,
              ),
              Text(
                "Please Wear a mask to proceed",
                style: TextStyle(fontSize: 30),
              )
            ],
          );
  }
}
