// Classifier Variable
var classifier;
// Model URL
var imageModelURL = 'https://teachablemachine.withgoogle.com/models/BlP1OZ89F/';

// Video
var video1;
var flippedVideo;
// To store the classification
var label = "";

function loadAllModels(video){
    if(classifier == null){
        console.error("first load");
        classifier = ml5.imageClassifier(imageModelURL + 'model.json');
    }

    video1 = video;
    //flippedVideo = ml5.flipImage(video)
    setInterval(async () => {
          classifier.classify(video1, gotResult);
        }, 100)
}

function gotResult(error, results) {
  // If there is an error
  if (error) {
    console.error(error);
    return;
  }
  // The results are in an array ordered by confidence.
  // console.log(results[0]);
  label = results[0].label;
  if(label.localeCompare("Correct: Mask On") == 0)
  {
    window.dispatchEvent(new Event('maskDetectionSuccess'));
  }
  else
  {
    window.dispatchEvent(new Event('maskDetectionFail'));
  }
}

