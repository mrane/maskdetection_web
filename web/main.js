// Classifier Variable
var classifier;
// Model URL
var imageModelURL = 'https://teachablemachine.withgoogle.com/models/BlP1OZ89F/';

// Video
var video;
var flippedVideo;
// To store the classification
var label = "";

// Load the model first
function preload() {
  classifier = ml5.imageClassifier(imageModelURL + 'model.json');
}

function setup() {
  createCanvas(640, 520);
  // Create the video
  video = createCapture(VIDEO);
  console.log("video = "+ typeof video)
  video.size(640, 520);
  video.hide();

  flippedVideo = ml5.flipImage(video)
  console.log("flippedVideo = "+ typeof flippedVideo)
  // Start classifying
  classifyVideo();
  //whoFunction();
}

function draw() {
  background(0);
  // Draw the video
  image(flippedVideo, 0, 0);

  // Draw the label
  fill('red');
  textSize(70);
  textAlign(CENTER);
  text(label, width / 2, height - 4);
}

// Get a prediction for the current video frame
function classifyVideo() {
  flippedVideo = ml5.flipImage(video)
  classifier.classify(flippedVideo, gotResult);
}

// When we get a result
function gotResult(error, results) {
  // If there is an error
  if (error) {
    console.error(error);
    return;
  }
  // The results are in an array ordered by confidence.
  // console.log(results[0]);
  label = results[0].label;
  console.log(label);
  // Classifiy again!
  classifyVideo();
}

function whoFunction() {
  var x = document.createElement("IFRAME");
  x.setAttribute("src", "https://www.who.int/emergencies/diseases/novel-coronavirus-2019/advice-for-public/when-and-how-to-use-masks");
  x.style.width = '640px';
  x.style.height = '520'
  document.body.appendChild(x);
}